package ru.blogspot.eagle_apps.infinite_calendar.ui.adapters

interface SectionAdapter {
  fun getSectionCount(): Int
  fun getItemsInSection(section: Int): Int
}
package ru.blogspot.eagle_apps.infinite_calendar.ui.sample

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import org.threeten.bp.LocalDate
import ru.blogspot.eagle_apps.infinite_calendar.ui.adapters.SectionHeaderAdapter
import ru.blogspot.eagle_apps.infinite_calendar.ui.adapters.SectionHeaderMapManager
import ru.blogspot.eagle_apps.infinitecalendar.R
import timber.log.Timber

class CalendarAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
  //val startDate = LocalDateTime.of(2016, 4, 1, 0, 0, 0)
  //val dayOffset = startDate.dayOfWeek.value - 1

  val year = 2016
  val CALENDAR_ITEM = R.layout.calendar_item
  val CALENDAR_MONTH = R.layout.calendar_month_header

  private val sectionManager = SectionHeaderMapManager(YearSectionAdapter(year))

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    val itemPosition = sectionManager.getItemPosition(position)
    val sectionHolder = holder as SectionHolder
    sectionHolder.bind(itemPosition.section, itemPosition.position, position)
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
    val itemView = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
    val viewHolder: RecyclerView.ViewHolder = when(viewType) {
      CALENDAR_ITEM -> CalendarViewHolder(itemView)
      CALENDAR_MONTH -> MonthViewHolder(itemView)
      else -> throw RuntimeException("Unknown view type: $viewType")
    }
    return viewHolder
  }

  override fun getItemCount(): Int {
    return sectionManager.getItemCount()
  }

  override fun getItemViewType(position: Int): Int {
    val itemPosition = sectionManager.getItemPosition(position)

    return if (itemPosition.position < 0) CALENDAR_MONTH else CALENDAR_ITEM
  }

  interface SectionHolder {
    fun bind(section: Int, index: Int, absolute: Int)
  }

  inner class CalendarViewHolder(view: View) : RecyclerView.ViewHolder(view), SectionHolder {
    val calendarText = view.findViewById(R.id.calendarText) as TextView

    init {
      view.setOnClickListener {
        Timber.d("Click: $adapterPosition")
      }
    }

    override fun bind(section: Int, index: Int, absolute: Int) {
      val startDate = LocalDate.of(year, section + 1, 1)
      val dayOffset = startDate.dayOfWeek.value - 1
      val day = index.toLong() - dayOffset
      if (day < 0) {
        calendarText.text = ""
      } else {
        val date = startDate.plusDays(day)
        calendarText.text = date.dayOfMonth.toString()
      }
    }
  }

  inner class MonthViewHolder(view: View) : RecyclerView.ViewHolder(view), SectionHolder {
    val calendarText = view.findViewById(R.id.calendarText) as TextView

    override fun bind(section: Int, index: Int, absolute: Int) {
      val month = itemView.context.resources.getStringArray(R.array.months)[section]
      calendarText.text = month
    }
  }

  class YearSectionAdapter(private val year: Int) : SectionHeaderAdapter {
    private val TOTAL_MONTH = 12
    override fun shouldShowHeader(section: Int): Boolean {
      return true
    }

    override fun getSectionCount(): Int {
      return TOTAL_MONTH
    }

    override fun getItemsInSection(section: Int): Int {
      val date = LocalDate.of(year, section + 1, 1)
      return date.month.maxLength() + date.dayOfWeek.value - 1
    }
  }
}
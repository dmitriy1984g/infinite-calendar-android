package ru.blogspot.eagle_apps.infinite_calendar.ui.sample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import ru.blogspot.eagle_apps.infinitecalendar.R
import timber.log.Timber


class SampleActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_sample)

    Timber.plant(Timber.DebugTree())

    val recyclerView = findViewById(R.id.recyclerView) as RecyclerView
    setupRecyclerView(recyclerView)
  }

  private fun setupRecyclerView(recyclerView: RecyclerView) {
    val adapter = CalendarAdapter()
    val layout = GridLayoutManager(this, 7)
    layout.spanSizeLookup = CalendarSpanSizeLookup(adapter)
    recyclerView.layoutManager = layout

    recyclerView.adapter = adapter
  }

  class CalendarSpanSizeLookup(private val adapter: CalendarAdapter)
  : GridLayoutManager.SpanSizeLookup() {
    override fun getSpanSize(position: Int): Int {
      return when(adapter.getItemViewType(position)) {
        adapter.CALENDAR_ITEM -> 1
        adapter.CALENDAR_MONTH -> 7
        else -> 1
      }
    }
  }
}
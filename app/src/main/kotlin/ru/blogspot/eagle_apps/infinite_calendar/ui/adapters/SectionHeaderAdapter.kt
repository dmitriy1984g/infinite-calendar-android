package ru.blogspot.eagle_apps.infinite_calendar.ui.adapters

interface SectionHeaderAdapter : SectionAdapter {
  fun shouldShowHeader(section: Int): Boolean
}
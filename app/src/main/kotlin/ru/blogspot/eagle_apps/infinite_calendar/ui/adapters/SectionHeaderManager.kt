package ru.blogspot.eagle_apps.infinite_calendar.ui.adapters


class SectionHeaderMapManager(private val adapter: SectionHeaderAdapter) : SectionManager {

  private var sectionMap: Map<Int, Int> = emptyMap()
  private var itemsCount: Int = -1

  override fun getItemCount(): Int {

    if (itemsCount < 0) {
      val map = mutableMapOf<Int, Int>()
      itemsCount = 0
      for (section: Int in 0..adapter.getSectionCount() - 1) {
        map.put(section, itemsCount)
        if (adapter.shouldShowHeader(section)) {
          itemsCount++
        }
        itemsCount += adapter.getItemsInSection(section)
      }

      sectionMap = map.toSortedMap()
    }

    return itemsCount
  }

  override fun getItemPosition(position: Int): ItemPosition {
    val sectionEntry = getSectionEntry(position)
    val pos: Int
    if (adapter.shouldShowHeader(sectionEntry.key)) {
      pos = position - sectionEntry.value - 1
    } else {
      pos = position - sectionEntry.value
    }

    return ItemPosition(sectionEntry.key, pos)
  }

  private fun getSectionEntry(position: Int): Map.Entry<Int, Int> {
    var prevSection: Map.Entry<Int, Int>? = null
    for(section: Map.Entry<Int, Int> in sectionMap) {
      if (section.value > position) {
        break
      }
      prevSection = section
    }
    return prevSection ?: throw RuntimeException("Section not found, position: $position")
  }

  override fun onDataSetChanged() {
    itemsCount = -1
  }
}
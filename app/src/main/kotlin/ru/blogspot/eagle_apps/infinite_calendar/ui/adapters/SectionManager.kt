package ru.blogspot.eagle_apps.infinite_calendar.ui.adapters

interface SectionManager {
  fun getItemCount(): Int
  fun getItemPosition(position: Int): ItemPosition
  fun onDataSetChanged()
}

data class ItemPosition(val section: Int, val position: Int)

class SectionMapManager(private val adapter: SectionAdapter) : SectionManager {
  // section, sectionIndex
  private var sectionMap: Map<Int, Int> = emptyMap()
  private var itemsCount: Int = -1

  override fun getItemCount(): Int {
    if (itemsCount < 0) {
      val map = mutableMapOf<Int, Int>()
      itemsCount = 0
      for (section: Int in 0..adapter.getSectionCount() - 1) {
        map.put(section, itemsCount)
        itemsCount += adapter.getItemsInSection(section)
      }

      sectionMap = map.toSortedMap()
    }

    return itemsCount
  }

  private fun getSectionEntry(position: Int): Map.Entry<Int, Int> {
    var prevSection: Map.Entry<Int, Int>? = null
    for(section: Map.Entry<Int, Int> in sectionMap) {
      if (section.value > position) {
        break
      }
      prevSection = section
    }
    return prevSection ?: throw RuntimeException("Section not found, position: $position")
  }

  override fun getItemPosition(position: Int): ItemPosition {
    val sectionEntry = getSectionEntry(position)
    return ItemPosition(sectionEntry.key, position - sectionEntry.value)
  }

  override fun onDataSetChanged() {
    itemsCount = -1
  }
}